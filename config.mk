#----------------------------------------------------------------------
# Common Config variables and targets.
# Includes configurations from configs directory.
#----------------------------------------------------------------------

#---------------------------------------------------------------------
# Edit these for a new build environment

# Project Architecture and Vendor
ARCH := arm

# Hardware
# Specified by the *Box project, all lowercase.
HW = pibox
ifeq ($(HW),pisentry)
override HW := pibox
PLATFORM    := pisentry
else
PLATFORM    := any
endif

# Build Version ID
BLD_VERSION := $(shell cat version.txt)

#---------------------------------------------------------------------
# End of Build Configurable options
#---------------------------------------------------------------------
# Number of parallel jobs.  Override this on the command line.
JOBS = 4

# Who am I?
UID = $(shell id -u)

#---------------------------------------------------------------------
# Target names - these are also used with a "." prefix as
# empty target files for various build sections.
# TARGETS gets updated in each component's .cfg file.

INIT_T          := init
TARGETS         = 

#---------------------------------------------------------------------
# Directories: 
# The build and archive downloads are kept in parallel directories from
# the source tree so hg status won't get confused by all the new files.
TOPDIR              := $(shell pwd)
SRCDIR              := $(TOPDIR)/src
ARCDIR              := $(TOPDIR)/../archive
BLDDIR              := $(TOPDIR)/../bld
PKGDIR              := $(TOPDIR)/../pkg
SCRIPTDIR           := $(TOPDIR)/scripts

#---------------------------------------------------------------------
# Macro to find toolchain prefix
define GET_TC_PREFIX
$(shell
	basename $(shell ls -1 $1/bin/*gcc | sed 's/-gcc//')
)
endef

#---------------------------------------------------------------------
# These variables can be set as environment variables on the "make" command line

# The location of the cross toolchain is set with XI
ifeq ($(XI),)
XCC_PREFIXDIR       = /opt/rpiTC
else
XCC_PREFIXDIR       = $(XI)
endif
XCC_PREFIX          := $(call GET_TC_PREFIX, $(XCC_PREFIXDIR))
CROSS_COMPILER      := $(XCC_PREFIXDIR)/bin/$(XCC_PREFIX)-gcc

# Where the PiBox rootfs is unpacked is set with ROOTFS
XBMC_ROOTFS         = $(ROOTFS)

# Where we can find the opkg-build utility is set with OPKG
ifeq ($(OPKG),)
OPKG_DIR			= /usr/local/bin
else
OPKG_DIR			= $(OPKG)
endif

#---------------------------------------------------------------------
# Include the configs directory files after the common configs
# Note: Order here is important

include configs/piboxwww.cfg

#---------------------------------------------------------------------
# Include the component makefiles
# Note: Order here is important
include configs/piboxwww.mk

#---------------------------------------------------------------------
# Config display target
showconfig:
	@$(MSG3) Common Configuration $(EMSG)
	@echo "Components           :$(TARGETS)"
	@echo "Version              : $(BLD_VERSION)"
	@echo "ARCH                 : $(ARCH)"
	@echo "HW                   : $(HW)"
	@echo "PLATFORM             : $(PLATFORM)"
	@echo "SRCDIR               : $(SRCDIR)"
	@echo "ARCDIR               : $(ARCDIR)"
	@echo "BLDDIR               : $(BLDDIR)"
	@echo "XCC_PREFIXDIR        : $(XCC_PREFIXDIR)"
	@echo "XCC_PREFIX           : $(XCC_PREFIX)"
	@echo "CROSS_COMPILER       : $(CROSS_COMPILER)"
	@echo "OPKG_DIR             : $(OPKG_DIR)"

