## Synopsis

piboxwww is a package providing a PHP-based Web interface to PiBox.  It offers user management for the web server (but not PiBox itself), network management via communication to piboxd and libpnc, and a webcam interface (also via piboxd and the mjpg-streamer PiBox package).

## Build

piboxwww has a GNU Make build system whose primary purpose is to package the web service.

The build depends on some environment variables being set.  Copy the file docs/bashsetup.sh to another directory and edit it as appropriate.  After editing, source the script:

    . <path_to_script>/bashsetup.sh

Then setup your environment by running the function in that script:

    piboxwww

Now you're ready to retrieve the source.  Run the following command to see how to clone the source tree:

    cd?

This will tell you exactly what to do.  After you clone the source you can do a complete build just by typing:

    sudo make pkg

To clean up:

    sudo make clobber

To get additional help on available targets, use the following command:

    make help

This will display all the available targets and environment variables with explanations on how to use them.

## Installation

Piboxwww is packaged in an opkg format.  After building, look in the opkg directory for an .opk file.  This is the file to be installed on the target system.

To install the package on the target, copy the file to the system or the SD card that is used to boo the system.  The use the following command.

    opkg install <path to file>/piboxwww_1.0_arm.opk

If the package has been installed previously, use the following command to reinstall it.

    opkg --force-reinstall install <path to file>/piboxwww_1.0_arm.opk

## Contributors

For more information on building, see the wiki:
http://www.graphics-muse.org/wiki/pmwiki.php

## License

0BSD

