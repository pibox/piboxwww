# ---------------------------------------------------------------
# Build piboxwww
# ---------------------------------------------------------------

root-verify:
	@if [ $(UID) -ne 0 ]; then \
		$(MSG11) "You must be root." $(EMSG); \
		exit 1; \
	fi

opkg-verify:
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

# Retrieve package
.$(WWW_T)-get $(WWW_T)-get: 
	@$(MSG) "================================================================"
	@$(MSG2) "Retrieving files" $(EMSG)
	@$(MSG) "================================================================"
	@mkdir -p $(WWW_SRCDIR)
	@cp -r $(DIR_WWW) $(WWW_SRCDIR)
	@touch .$(subst .,,$@)

.$(WWW_T)-get-patch $(WWW_T)-get-patch: .$(WWW_T)-get
	@touch .$(subst .,,$@)

# Unpack packages
.$(WWW_T)-unpack $(WWW_T)-unpack: .$(WWW_T)-get-patch
	@touch .$(subst .,,$@)

# Apply patches
.$(WWW_T)-patch $(WWW_T)-patch: .$(WWW_T)-unpack
	@touch .$(subst .,,$@)

.$(WWW_T)-init $(WWW_T)-init: .$(WWW_T)-patch
	@touch .$(subst .,,$@)

.$(WWW_T)-config $(WWW_T)-config: 

# Build the package
$(WWW_T): .$(WWW_T)

.$(WWW_T): .$(WWW_T)-init 
	@make --no-print-directory $(WWW_T)-config
	@touch .$(subst .,,$@)

$(WWW_T)-files:
	@$(MSG) "================================================================"
	@$(MSG2) "WWW Files" $(EMSG)
	@$(MSG) "================================================================"
	@ls -l $(WWW_SRCDIR)

# Package it as an opkg 
opkg pkg $(WWW_T)-pkg: .$(WWW_T)
	@make --no-print-directory root-verify opkg-verify
	@mkdir -p $(PKGDIR)/opkg/piboxwww/CONTROL
	@mkdir -p $(PKGDIR)/opkg/piboxwww/home/httpd/pibox
	@mkdir -p $(PKGDIR)/opkg/piboxwww/etc/init.d
	@cp -ar $(WWW_SRCDIR)/piboxwww/* $(PKGDIR)/opkg/piboxwww/home/httpd/pibox
ifeq ($(PLATFORM),pisentry)
	@sed -i '/settings/d' $(PKGDIR)/opkg/piboxwww/home/httpd/pibox/php/frontpage.php
endif
	@cp -ar $(SRCDIR)/scripts/* $(PKGDIR)/opkg/piboxwww/etc/init.d
	@rm -f $(PKGDIR)/opkg/piboxwww/home/httpd/pibox/images/*.xcf
	@cp $(SRCDIR)/opkg/control $(PKGDIR)/opkg/piboxwww/CONTROL/control
	@cp $(SRCDIR)/opkg/postinst $(PKGDIR)/opkg/piboxwww/CONTROL/preinst
	@cp $(SRCDIR)/opkg/postinst $(PKGDIR)/opkg/piboxwww/CONTROL/postinst
	@cp $(SRCDIR)/opkg/prerm $(PKGDIR)/opkg/piboxwww/CONTROL/prerm
	@cp $(SRCDIR)/opkg/debian-binary $(PKGDIR)/opkg/piboxwww/CONTROL/debian-binary
	@sed -i 's%\[VERSION\]%'`cat $(TOPDIR)/version.txt`'%' $(PKGDIR)/opkg/piboxwww/CONTROL/control
	@chmod +x $(PKGDIR)/opkg/piboxwww/CONTROL/preinst
	@chmod +x $(PKGDIR)/opkg/piboxwww/CONTROL/postinst
	@chmod +x $(PKGDIR)/opkg/piboxwww/CONTROL/prerm
	@chown -R root.root $(PKGDIR)/opkg/piboxwww/
	@cd $(PKGDIR)/opkg/ && $(OPKG_DIR)/opkg-build -O piboxwww
	@cp $(PKGDIR)/opkg/*.opk $(PKGDIR)/
	@rm -rf $(PKGDIR)/opkg

# Clean the packaging
pkg-clean $(WWW_T)-pkg-clean:
	@if [ "$(PKGDIR)" != "" ] && [ -d "$(PKGDIR)" ]; then rm -rf $(PKGDIR); fi

# Clean out a cross compiler build but not the CT-NG package build.
$(WWW_T)-clean: $(WWW_T)-pkg-clean
	@if [ "$(WWW_SRCDIR)" != "" ] && [ -d "$(WWW_SRCDIR)" ]; then rm -rf $(WWW_SRCDIR); fi
	@rm -f .$(WWW_T) .$(WWW_T)-get

# Clean out everything associated with WWW
$(WWW_T)-clobber: 
	@rm -rf $(BLDDIR) 
	@rm -f .$(WWW_T)-config .$(WWW_T)-init .$(WWW_T)-patch \
		.$(WWW_T)-unpack .$(WWW_T)-get .$(WWW_T)-get-patch
	@rm -f .$(WWW_T) 

