<!--  Hide from old browsers.

    /* 
     * ==================================
     * PiBox:  User PW Change Page Javascript
     * ==================================
     */

    /* 
     * ----------------------------------
     * Global variables
     * ----------------------------------
     */

    /* 
     * ----------------------------------
     * Utility functions
     * ----------------------------------
     */

    // Pause for the specified time.
    function pause(ms) 
    {
        ms += new Date().getTime();
        while (new Date() < ms){}
    } 

    // A jQuery dialog window
    function pwDialog( dTitle, dText )
    {
        if ( dText != "" )
            $( "#dialog" ).html( dText );
        else
            $( "#dialog" ).html( "Notice" );
        $( "#dialog" ).dialog( "option", "title", dTitle );
        $( "#dialog" ).dialog(
            {
   				closeOnEscape: false,
   				open: function(event, ui) { $(".ui-dialog-titlebar-close", this.parentNode).hide(); },
                modal: true,
                draggable: false,
                resizable: false,
                position: ['center', 'center'],
                show: 'blind',
                hide: 'blind',
                width: 400,
                dialogClass: 'ui-dialog'
            });
        $( "#dialog" ).dialog("open");
    }

    /* 
     * ----------------------------------
     * UI handling 
     * ----------------------------------
     */

    // Go back to home page
    function goHome() 
    {
        window.location = '/';
    }

    // Save password
    // This sends the username/pw to piboxd to do the update.
    function savePW( ) 
    {
        username = $( "#userName" ).val();
        userpw   = $( "#userPW" ).val();
        if ( userpw.length<7 )
        {
            msg = "Password must be 7 or more characters";
            pwDialog( "Error", msg);
            return;
        }
        // Pass the username/pw to the handler
        $.post( "/php/pibox.php?function=savepw", 
                {
                    username: username,
                    pw: userpw
                }
            );

        setTimeout(function() {
                // Redirect to the users page again.
                window.location = '/';
            },
            6000);
        pwDialog( "Saving", "Updating user</br>You will be redirected in a moment...");
    }

    // Reset just returns to the front page.
    function resetPW() 
    {
        window.location = '/';
    }

    // Show or hide the password field.
    function userPWShow()
    {
        var isChecked = $("#UPWshow").prop('checked');
        if (isChecked) {
            $('#userPWhidden').val( $("#userPW").val() );
            $('#userPW').hide();
            $('#userPWhidden').show();
        }
        else {
            $('#userPW').val( $("#userPWhidden").val() );
            $('#userPW').show();
            $('#userPWhidden').hide();
        }
    }

    // Update pw fields when changed.
    function userpwChanged(type)
    {
        $( "#dialog" ).dialog("close");
        if ( type == "v" ) { $('#userPWhidden').val( $("#userPW").val() ); }
        else               { $('#userPW').val( $("#userPWhidden").val() ); }
    }

-->
