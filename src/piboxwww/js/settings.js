<!--  Hide from old browsers.

    /* 
     * ==================================
     * PiBox:  Settings Page Javascript
     * ==================================
     */

    /* 
     * ----------------------------------
     * Global variables
     * ----------------------------------
     */
    var currentTab;

    /* 
     * ----------------------------------
     * Utilities 
     * ----------------------------------
     */
    function checkIP(ipaddr) 
    {
        validIP = true; 
        fields = ipaddr.split(".");
        if( fields.length == 4 )
        {
            for(i=0; i<4; i++) 
            {
                num = parseInt(fields[i]);
                if(num <0 || num > 255)
                {
                    validIP = false;
                    break;
                }
            }
        }
        else
            validIP = false;
        return validIP;
    }

    /* 
     * ----------------------------------
     * UI handling 
     * ----------------------------------
     */

    // idTabs configuration for tabbed interface 
    // requires jQuery idTabs: http://www.sunsean.com/idTabs/
    $(function() {
        $( "#tabs" ).tabs();
    });
    $( "#buttonBar" ).buttonSet();

    // Go back to home page
    function goHome() 
    {
        window.location = '/';
    }

    // Set network mode
    var netMode = "unknown";
    function setNetMode( newMode ) 
    {
        // Save mode - we'll use it when saving data to server.
        netMode = newMode;
        if ( newMode == "wap" )
        {
            $("#accessPointT").click(); 
            setTab("accessPoint");
        }
        else
        {
            $("#wirelessT").click(); 
            setTab("wireless");
        }

        // Always save the current mode (wireless client or access point)
        $.post( "/php/pibox.php?function=settings", 
            {
                subPage: 'setMode',
                mode: netMode
            }
        );
    }

    // Setup specified tab.
    function setTab( name )
    {   
        currentTab = name;
        setTabHighlight(name);
        if ( name == "ipv4" )
        {   
        }
        else  if ( name == "wireless" )
        {   
            $.get( "/php/pibox.php?function=settings&tab=wireless",
                function( data ) {
                    $( "#wirelessTab" ).html( data );
                }
            );
        }
        else
        {   
            $( "#macFields" ).html("");
        	$("#MACshow").prop('checked', false);
            $.get( "/php/pibox.php?function=settings&tab=accessPoint",
                function( data ) {
                    $( "#accessPointTab" ).html( data );
            		$.get( "/php/pibox.php?function=settings&getACLState=1",
                		function( data ) {
                    		if ( data == "1" )
                    		{
                        		document.getElementById("MACshow").checked = true;
                    		}
                    		else
                    		{
                        		document.getElementById("MACshow").checked = false;
                    		}
                        	macShow();
                		}
            		);
                }
            );
        }
    }

    // Set tab highlighting.
    function setTabHighlight( name )
    {   
        if ( name == "ipv4" )
        {   
            $( "#ipv4T" ).css( 'color', "#fff" );    
            $( "#wirelessT" ).css( 'color', "#888" );
            $( "#accessPointT" ).css( 'color', "#888" );
        }
        else  if ( name == "wireless" )
        {   
            $( "#ipv4T" ).css( 'color', "#888" );    
            $( "#wirelessT" ).css( 'color', "#fff" );
            $( "#accessPointT" ).css( 'color', "#888" );
        }
        else
        {   
            $( "#ipv4T" ).css( 'color', "#888" );    
            $( "#wirelessT" ).css( 'color', "#888" );
            $( "#accessPointT" ).css( 'color', "#fff" );
        }
    }

    // Manage a dialog window
    function doDialog( dTitle, dText )
    {
        if ( dText != "" )
            $( "#dialog" ).html( dText );
        else
            $( "#dialog" ).html( "No response from server." );
        $( "#dialog" ).dialog( "option", "title", dTitle );
        $( "#dialog" ).dialog(
            {
                modal: true,
                closeText: "Ok",
                draggable: false,
                resizable: false,
                position: ['center', 'center'],
                show: 'blind',
                hide: 'blind',
                width: 400,
                dialogClass: 'ui-dialog'
            });
        $( "#dialog" ).dialog("open");
    }

    // When interface option is changed.
    function ifChanged( newIF ) 
    {
        window.location = '/php/pibox.php?function=settings&tab=ipv4&interface=' + newIF;
    }

    // When address type option is changed.
    function addressTypeChanged( target ) 
    {
        if ( target.value == "DHCP" )
        {
            $( "#staticFields" ).html("");
        }
        else 
        {
            interface = $("#Interfaces option:selected").text().trim();
            $.get( "/php/pibox.php?function=settings&subPage=staticFields&interface=" + interface, 
                function( data ) {
                    $( "#staticFields" ).html( data );
                }
            );
        }
    }

    // Save depends on which page we're on.
    function saveIt( ) 
    {
        if ( currentTab == "ipv4" )
            saveInterface();
        else if ( currentTab == "wireless" )
            saveWireless();
        else if ( currentTab == "accessPoint" )
            saveAccessPoint();
    }

    // Save Interface page
    function saveInterface( ) 
    {
        // Get interface being updated.
        iface= $("#Interfaces option:selected").text().trim();

        // Get Enabled state
        selected = $("#IFEnabled input[type='radio']:checked");
        if (selected.length > 0) {
            estate = selected.val();
        }

        // Get config type (DHCP or static)
        ctype = $("#AddressType option:selected").text().trim();

        // If DHCP or disabling interface, do that immediately without validation of other fields.
        if ( (estate == "no") || (ctype == "DHCP") )
        {
            $.post( "/php/pibox.php?function=settings", 
                    {
                        subPage: 'save',
                        type: 'ipv4',
                        interface: iface,
                        enabled: estate,
                        config: ctype
                    },
                    function(data, status) {
                        doDialog( "Save", data);
                    }
                );
            return;
        }

        // If enabled and static:
        addr = $( "#IPAddress" ).val();
        nm   = $( "#Netmask" ).val();
        gw   = $( "#Gateway" ).val();
        d0   = $( "#DNS0" ).val();
        d1   = $( "#DNS1" ).val();
        d2   = $( "#DNS2" ).val();

        // Validate fields
        if ( !checkIP(addr) ) {
            doDialog( "Error", "Invalid IP address");
            return;
        }
        if ( !checkIP(nm) ) {
            doDialog( "Error", "Invalid netmask");
            return;
        }
        if ( !checkIP(gw) ) {
            doDialog( "Error", "Invalid gateway address");
            return;
        }
        if ( (d0.length>0) && !checkIP(d0) ) {
            doDialog( "Error", "Invalid DNS 0 address");
            return;
        }
        if ( (d1.length>0) && !checkIP(d1) ) {
            doDialog( "Error", "Invalid DNS 1 address");
            return;
        }
        if ( (d2.length>0) && !checkIP(d2) ) {
            doDialog( "Error", "Invalid DNS 2 address");
            return;
        }

        // Remove duplicates for DNS entries
        // No need to bubble up to fill empty slots - piboxd deals with that.
        if ( d0 == d1 ) d1 = "";
        if ( d0 == d2 ) d2 = "";
        if ( d1 == d2 ) d2 = "";

        $.post( "/php/pibox.php?function=settings", 
                {
                    subPage: 'save',
                    type: 'ipv4',
                    interface: iface,
                    enabled: estate,
                    config: ctype,
                    ipaddr: addr,
                    netmask: nm,
                    gateway: gw,
                    dns0: d0,
                    dns1: d1,
                    dns2: d2
                },
                function(data, status) {
                    doDialog( "Save", data);
                }
            );
    }

    // Save Wireless page
    function saveWireless( ) 
    {
        wcssid = $( "#WCSSID" ).val();
        wcsec  = $( "#WCSEC option:selected").text().trim();
        wcpw   = $( "#WCPW" ).val();
        if ( wcssid.length<4 )
        {
            doDialog( "Error", "SSID must be 4 or more characters");
            return;
        }
        if ( wcpw.length<7 )
        {
            doDialog( "Error", "Password must be 7 or more characters");
            return;
        }
        $.post( "/php/pibox.php?function=settings", 
                {
                    subPage: 'save',
                    type: 'wireless',
                    ssid: wcssid,
                    security: wcsec,
                    pw: wcpw
                },
                function(data, status) {
                    doDialog( "Save", data);
                }
            );
    }

    // Save Access Point page
    function saveAccessPoint( ) 
    {
        apssid    = $( "#APSSID" ).val();
        apch      = $( "#APCH option:selected").text().trim();
        appw      = $( "#APPW" ).val();
        apbase    = $( "#APBASE" ).val();
        isChecked = $("#MACshow").prop('checked');

        if ( !checkIP(apbase) ) {
            doDialog( "Error", "Invalid base address");
            return;
        }

        // Gather 
        if ( isChecked )
        {
            // This is ugly but will have to wait till I get
            // better at jQuery/Javascript for improvement.
            mac1 = $( "#mac1" ).val();
            mac2 = $( "#mac2" ).val();
            mac3 = $( "#mac3" ).val();
            mac4 = $( "#mac4" ).val();
            mac5 = $( "#mac5" ).val();
            mac6 = $( "#mac6" ).val();
            mac7 = $( "#mac7" ).val();
            mac8 = $( "#mac8" ).val();

            var regex = /^([0-9a-fA-F]{2}[:]){5}([0-9a-fA-F]{2})$/;
            if ( (mac1.length > 0) && (!regex.test(mac1)) ) { doDialog("Invalid MAC",mac1); return; }
            if ( (mac2.length > 0) && (!regex.test(mac2)) ) { doDialog("Invalid MAC",mac2); return; }
            if ( (mac3.length > 0) && (!regex.test(mac3)) ) { doDialog("Invalid MAC",mac3); return; }
            if ( (mac4.length > 0) && (!regex.test(mac4)) ) { doDialog("Invalid MAC",mac4); return; }
            if ( (mac5.length > 0) && (!regex.test(mac5)) ) { doDialog("Invalid MAC",mac5); return; }
            if ( (mac6.length > 0) && (!regex.test(mac6)) ) { doDialog("Invalid MAC",mac6); return; }
            if ( (mac7.length > 0) && (!regex.test(mac7)) ) { doDialog("Invalid MAC",mac7); return; }
            if ( (mac8.length > 0) && (!regex.test(mac8)) ) { doDialog("Invalid MAC",mac8); return; }

            $.post( "/php/pibox.php?function=settings", 
                {
                    subPage: 'save',
                    type: 'accessPoint',
                    ssid: apssid,
                    channel: apch,
                    pw: appw,
                    base: apbase,
                    usemac: 1,
                    mac1: mac1,
                    mac2: mac2,
                    mac3: mac3,
                    mac4: mac4,
                    mac5: mac5,
                    mac6: mac6,
                    mac7: mac7,
                    mac8: mac8
                },
                function(data, status) {
                    doDialog( "Save", data);
                }
            );
        }
        else
        {
            $.post( "/php/pibox.php?function=settings", 
                {
                    subPage: 'save',
                    type: 'accessPoint',
                    ssid: apssid,
                    channel: apch,
                    base: apbase,
                    pw: appw,
                },
                function(data, status) {
                    doDialog( "Save", data);
                }
            );
        }
    }

    // Reset current configuration.
    function reset() 
    {
        iface= $("#Interfaces option:selected").text().trim();
        window.location = '/php/pibox.php?function=settings&tab=ipv4&interface=' + iface;
    }

    // When MAC filtering check button is changed.
    function macShow() 
    {
        var isChecked = $("#MACshow").prop('checked');
        if ( isChecked )
        {
            $.get( "/php/pibox.php?function=getMACFields", 
                function( data ) {
                    $( "#macFields" ).html( data );
                }
            );
        }
        else 
        {
            $( "#macFields" ).html("");
        }
    }

    /* 
     * ----------------------------------
     * Page Setup
     * ----------------------------------
     */
    function loadSetup() 
    {
        // Clear wireless and accessPoint div's if we're called from index.
        if ( window.location.href.indexOf("&interface") == -1 )
        {
            $( "#wirelessTab" ).html( "" );
            $( "#accessPointTab" ).html( "" );
        }

        // Set the current mode.
        $.get( "/php/pibox.php?function=settings&subPage=getMode", 
                function(data) {
                    if ( data == "wap" )
                    {
                        $("#netType input[type='radio'][id='wapType']").prop('checked',true);
                    }
                    else
                    {
                        $("#netType input[type='radio'][id='wirelessType']").prop('checked',true);
                    }
                }
        );

        // Make the IPV4 tab highlighted.           
        setTabHighlight("ipv4");
        currentTab = "ipv4";

        // Create message dialog 
        $( "#dialog" ).dialog({ autoOpen: false });
    }

    // Show or hide the wireless client page's password field.
    function wcpwShow()
    {
        var isChecked = $("#WCPWshow").prop('checked');
        if (isChecked) {
            $('#WCPWhidden').val( $("#WCPW").val() );
            $('#WCPW').hide();
            $('#WCPWhidden').show();
        }
        else {
            $('#WCPW').val( $("#WCPWhidden").val() );
            $('#WCPW').show();
            $('#WCPWhidden').hide();
        }
    }

    // Update pw fields when matched pair changes.
    function wcpwChanged(type)
    {
        if ( type == "v" ) { $('#WCPWhidden').val( $("#WCPW").val() ); }
        else               { $('#WCPW').val( $("#WCPWhidden").val() ); }
    }

    // Show or hide the access point client page's password field.
    function appwShow()
    {
        var isChecked = $("#APPWshow").prop('checked');
        if (isChecked) {
            $('#APPWhidden').val( $("#APPW").val() );
            $('#APPW').hide();
            $('#APPWhidden').show();
        }
        else {
            $('#APPW').val( $("#APPWhidden").val() );
            $('#APPW').show();
            $('#APPWhidden').hide();
        }
    }

    // Update pw fields when matched pair changes.
    function appwChanged(type)
    {
        if ( type == "v" ) { $('#APPWhidden').val( $("#APPW").val() ); }
        else               { $('#APPW').val( $("#APPWhidden").val() ); }
    }
-->
