<!--  Hide from old browsers.

    /* 
     * ==================================
     * PiBox:  Modify, add or delete users
     * ==================================
     */

    /* 
     * ----------------------------------
     * UI handling 
     * ----------------------------------
     */

    // A jQuery dialog window
    function doDialog( dTitle, dText )
    {
        if ( dText != "" )
            $( "#dialog" ).html( dText );
        else
            $( "#dialog" ).html( "No response from server." );
        $( "#dialog" ).dialog( "option", "title", dTitle );
        $( "#dialog" ).dialog(
            {
                modal: true,
                closeText: "Ok",
                draggable: false,
                resizable: false,
                position: ['center', 'center'],
                show: 'blind',
                hide: 'blind',
                width: 400,
                dialogClass: 'ui-dialog'
            });
        $( "#dialog" ).dialog("open");
    }

    // Go back to home page
    function goHome() 
    {
        window.location = '/';
    }

    // Reset clears the new user text field.
    function resetUsers() 
    {
        $( "#username" ).val('');
    }

    // Add a new user by passing it to the userPW page.
    function addUser()
    {
        $username = $("#newuser").val();
        $.post( "/php/pibox.php?function=updateUser", 
            {
                username: $username
            },
            function(data, status) {
                // Replace the page 
                $('body').html(data);
            }
        );
    }

    // Delete a user 
    function deleteUser($username)
    {
        $('#msgBox').html('Working...');
        $.post( "/php/pibox.php?function=updateUser", 
            {
                username: $username,
                delete: 1 
            }
        );
        setTimeout(function() {
                // Clear the message box.
                $('#msgBox').html('');

                // Redirect to the users page again.
                window.location = '/php/pibox.php?function=users';
            },
            6000);
    }

    // Edit a user 
    function editUser($username)
    {
        $.post( "/php/pibox.php?function=updateUser", 
            {
                username: $username
            },
            function(data, status) {
                // Replace the page 
                $('#wrapper').html(data);
            }
        );
    }
-->
