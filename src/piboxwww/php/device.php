<?php

/*
 * ---------------------------------------------------------------
 * device.php
 * Handles commands sent to PiBox from a remote device.
 * ---------------------------------------------------------------
 */

ini_set('display_errors', 'On');
ini_set('error_log', '/var/log/piboxwww.log');
error_reporting(E_NOTICE);

/*
 * Debugging and other globally useful classes and functions.
 */
include "util.php";
$dbg = new PiboxLog();
// Uncomment this to enable debugging
$dbg->setLevel('DEBUG');

// PiBoid (Android remote control)
include "piboid.php";

/*
 * ---------------------------------------------------------------
 * Main routine - parse RESTful component and call appropriately.
 * ---------------------------------------------------------------
 */
function main()
{
    global $dbg;

    $dbg->info("Entered device.php");

    // The RESTful API specifies that c=<> contains the RESTful
    // component.  
    $rest=$_GET["c"];
    $dbg->info("RESTful query: " . $rest);
	$api = explode("/", $rest);

	// API format
	// 1: API version (v1, v2, etc.)
	// 2: source
	// 3-end: source specific commands

    if ( $api[2] == "piboid" )
    {
		// Always returns an empty string
        piboid($api);
		print "";
    }
    else
	{
    	$dbg->info("Invalid source: " . $api[1]);
	}
}

main();

?>
