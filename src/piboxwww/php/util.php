<?php

/*
 * ---------------------------------------------------------------
 * Utility classes and functions.
 * ---------------------------------------------------------------
 */

/*
 * ---------------------------------------------------------------
 * Simplistic debug utility.
 * ---------------------------------------------------------------
 */
class PiboxLog
{
    static private $iminit = NULL;

    private $logLevels = array(
                'OFF'   => 0,
                'ERROR' => 1,
                'WARN'  => 2,
                'INFO'  => 3,
                'DEBUG' => 4,
                'TRACE' => 5
                );
    static private $logFile = NULL;
    private $logLevel = 'OFF';  // Default debug level. Higher values mean more output

    public function __construct()
    {
        if ( !isset(self::$iminit) )
        {
            /* Only the first instance of this class will set the log file */
            self::$iminit = IMInit::Instance();
            self::$logFile = self::$iminit->get("logpath")."/piboxwww.log";
            error_log("Logfile: " . self::$logFile);
        }
    }

    // Print message with newline.
    private function out($request, $msg="")
    {
        if ( $msg=="" )
            return;

        if ( $this->logLevels[$request] > $this->logLevels[$this->logLevel] )
            return;

        if ( !strpos($msg, "\n") )
            $msg .= "\n";
        file_put_contents(self::$logFile, $request . ": " . $msg, FILE_APPEND | LOCK_EX);
    }

    // Change the log level
    public function setLevel ($newLevel) { 
        if ( isset($this->logLevels[$newLevel]) )
            $this->logLevel = $newLevel; 
        else
            file_put_contents(self::$logFile, "ERROR: Invalid level: " + $newLevel, FILE_APPEND | LOCK_EX);
    }

    // Basic logging functions
    public function error ($msg="") { $this->out('ERROR', $msg); }
    public function warn  ($msg="") { $this->out('WARN',  $msg); }
    public function info  ($msg="") { $this->out('INFO',  $msg); }
    public function debug ($msg="") { $this->out('DEBUG', $msg); }
    public function trace ($msg="") { $this->out('TRACE', $msg); }
}

/*
 * ---------------------------------------------------------------
 * Retrieve configured interface IP address.  We assume the first
 * one listed that is not the loopback is the one we want.
 * Do this instead of gethostbyname(gethostname()) because we don't
 * set up name resolution on Pibox.
 * ---------------------------------------------------------------
 */
function getMyIP()
{
    $command="/sbin/ifconfig | grep 'inet addr:' | grep -v \"127.0.0.1\" | head -1 | cut -d: -f2 | awk '{ print $1}'";
    $localIP = exec ($command);
    return $localIP;
}

/*
 * ---------------------------------------------------------------
 * Validate an IP address. 
 * Return the address if its valid.
 * Return -1 if it is invalid.
 * ---------------------------------------------------------------
 */
function ipCheck( $addr="0" )
{
    if ( filter_var($addr, FILTER_VALIDATE_IP)) 
    {
        return $addr;
    }
    else
    {
        return -1;
    }
}

/*
 * ---------------------------------------------------------------
 * Setup a socket connection to a local daemon
 * ---------------------------------------------------------------
 */
function buildSocket($nonBlock=0, $port)
{
    global $dbg;

    $address = gethostbyname('localhost');
    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    if ($socket === false) {
        $errMsg = "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
        $dbg->info("buildSocket: " + $errMsg);
        return $socket;
    }
    $result = socket_connect($socket, $address, $port);
    if ($result === false) {
        $errMsg = "socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n";
        $dbg->info("buildSocket: " + $errMsg);
        return $result;
    }
    return $socket;
}

// Socket to piboxd
function getSocket($nonblock=0)
{
    return buildSocket($nonblock, 13910);
}

// Setup a socket connection to the appmgr daemon.
function getAppMgrSocket()
{
    return buildSocket(0, 13912);
}

?>
