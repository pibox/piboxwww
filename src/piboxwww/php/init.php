<?php
/*
 * ---------------------------------------------------------------
 * Singleton used to establish global variables and environment
 * required for the rest of the web services.
 *
 * This is instantiated by ironman.php first, then that instance
 * can be used to retrieve global variables.
 *
 * Note: this does not use $GLOBALS.
 * ---------------------------------------------------------------
 */

/*
 * ---------------------------------------------------------------
 * Initialization class
 * ---------------------------------------------------------------
 */
final class IMInit
{
    /*
     * Private variables
     */
    static private $docr;
    static private $dd;

    /*
     * The debug stamp file should be 
     *   $TOPDIR/src/imwww/data/debug
     * in order to run in debug mode.
     * This is only useful for developers.
     */
    static private $debugStamp;

    /* Debug paths are relative to the document root */
    static private $debugLogPath;
    static private $debugWSPath;

    /* Run time paths */
    static private $runtimeLogPath  = "/var/log/lighttpd";
    static private $runtimeWSPath   = "/var/run/lighttpd";

    /* Initialize the paths array. */
    static private $paths;

    /*
     * -------------------------------------------------
     * Force single instance of the class.
     * -------------------------------------------------
     */

    /*
     * Call this method to get singleton
     * and initialize the paths array.
     * @return UserFactory
     */
    public static function Instance()
    {
        static $inst = null;
        if ($inst === null) 
        {
            $inst = new IMINit();
            self::$docr            = $_SERVER['DOCUMENT_ROOT'];
            self::$dd              = self::$docr."/data";
            self::$debugStamp      = self::$dd."/debug";
            self::$debugLogPath    = self::$dd."/log";
            self::$debugWSPath     = self::$dd."/ws";
            self::$paths = array(
                'wspath' => self::$runtimeWSPath,
                'logpath'=> self::$runtimeLogPath,
                );
            $GLOBALS['wspath'] = self::$runtimeWSPath;
            $GLOBALS['logpath'] = self::$runtimeLogPath;
        }
        return $inst;
    }

    /*
     * Private constructor so nobody else can instance it
     */
    private function __construct()
    {
    }

    /*
     * -------------------------------------------------
     * Private functions
     * -------------------------------------------------
     */

    /*
     * Remove a directory path and all its contents.
     */
    private function rrmdir($dir)
    {
        if (is_dir($dir))
        {
            $objects = scandir($dir); 
            foreach ($objects as $object)
            {
                if ($object != "." && $object != "..")
                {
                    if (is_dir($dir."/".$object))
                        rrmdir($dir."/".$object);
                    else
                        unlink($dir."/".$object); 
                } 
            }
            rmdir($dir); 
        } 
    }

    /*
     * -------------------------------------------------
     * Public functions
     * -------------------------------------------------
     */

    /* 
     * Set up our operational environment.
     */
    public function setEnv()
    { 
        static $once = 0;

        /* Don't allow setup more than once. */
        if ( $once == 1 )
            return;

        /* 
         * If the debug stamp is found then we're running in debug mode
         * directly out of the source tree (not in production).
         */
        if ( file_exists(self::$debugStamp) )
        {
            self::$paths['wspath']  = self::$debugWSPath;
            self::$paths['logpath'] = self::$debugLogPath;

            if ( ! file_exists(self::$debugWSPath) )
            {
                mkdir(self::$debugWSPath, 0700, true);
            }
            if ( ! file_exists(self::$debugLogPath) )
            {
                mkdir(self::$debugLogPath, 0700, true);
            }
        }
        else
        {
            /* 
             * Make sure the data directory doesn't exist 
             * if we're running in production mode.
             */
            $this->rrmdir(self::$dd);
        }

        $once = 1;
    }

    /*
     * Retrieve a path setting.
     */
    public function get($pathname)
    { 
        return IMInit::$paths["$pathname"];
    }
}

?>
