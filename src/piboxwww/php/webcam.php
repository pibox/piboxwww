<?php

/*
 * ---------------------------------------------------------------
 * Start the webcam stream and generate a web page with flash viewer
 * that will send heartbeats and stream teardown messages for 
 * that stream.
 * ---------------------------------------------------------------
 */
function webcam()
{
    # Get the resolution selection.
    $res=$_GET["res"];

    // Get my IP address - we should error check this!
    $ipaddr = getMyIP();

    // Build start request
    if ( $res == "low" )
    {
        // Use low resolution video
        $header = 0x00000301;
    }
    else if ( $res == "med" )
    {
        // Use medium resolution video
        $header = 0x00000401;
    }
    else if ( $res == "hd" )
    {
        // Use HD resolution video
        $header = 0x00000501;
    }
    else
    {
        // Use high resolution video
        $header = 0x00000101;
    }
    $size = 0x00000006;
    $filename = "webcam";
    $tmpl = "webcam.tmpl";

    // Generate a tag: http://php.net/manual/en/function.com-create-guid.php
    // This isn't used at the moment because it's not a great way to maintain state.
    $tag = sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', 
            mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), 
            mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

    // Setup connection to piboxd.
    $socket = getSocket();

    // Send start request to piboxd.
    // Binary data needs to be pack()'d, but not sting data.
    // Order here is important!
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $tag, 36);
    socket_write($socket, $filename, 6);
    socket_close($socket);

    // Build page from appropriate template.
    $page = file_get_contents($tmpl);
    $page = str_replace("[TAG]", $tag, $page);
    $page = str_replace("[IPADDR]", $ipaddr, $page);

    // Artificial wait - the player on the server can take a few seconds to start.
    // There is no simple way to sync it and a little wait for the user is no biggie.
    sleep(3);

    // Print page.
    print $page;
}

/*
 * ---------------------------------------------------------------
 * Use the TARG variable to send a heartbeat message
 * to the piboxd daemon.
 * ---------------------------------------------------------------
 */
function heartbeat()
{
    $tag=$_GET["tag"];

    // Build heartbeat request
    $header = 0x00000102;
    $size = 0x00000001;
    $filename = "x";

    // Setup connection to piboxd.
    $socket = getSocket();

    // Send start request to piboxd.
    // Binary data needs to be pack()'d, but not sting data.
    // Order here is important!
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $tag, 36);
    socket_write($socket, $filename, 1);
    socket_close($socket);

    die();
}

/*
 * ---------------------------------------------------------------
 * Use the TAG variable to send a teardown message
 * to the piboxd daemon.
 * ---------------------------------------------------------------
 */
function teardown()
{
    $tag=$_GET["tag"];

    // Build heartbeat request
    $header = 0x00000201;
    $size = 0x00000006;
    $filename = "webcam";

    // Setup connection to piboxd.
    $socket = getSocket();

    // Send start request to piboxd.
    // Binary data needs to be pack()'d, but not sting data.
    // Order here is important!
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $tag, 36);
    socket_write($socket, $filename, 6);
    socket_close($socket);

    die();
}

?>
