<?php

/*
 * ---------------------------------------------------------------
 * Read static network configuration (IP addr, Gateway, etc.)
 * ---------------------------------------------------------------
 */
function getMACFields( $interface )
{
    // Get the template for the mac fields
    global $dbg;
    $tmpl = "macfields.tmpl";
    $dbg->info("Reading template " . $tmpl);
    $page = file_get_contents($tmpl);

    // If necessary, populate the fields
    $idx = 1;
    if ( strcmp(getACLState(), "1") == 0 )
    {
        $dbg->info("MAC ACL enabled - getting addresses");
        $socket = getSocket(1);
        $header = 0x00010D03;   // MT_NET, MA_MFLIST, Get
        socket_write($socket, pack("I", $header), 4);
        usleep(20);

        // Read response
        // Format of response:
        // newline-separated list of mac addresses
        if ( false === ($buf=socket_read($socket, 2048, PHP_NORMAL_READ)) )
        {
            error_log("Failed reading response to MS_GETIF.");
            return -1;
        }

        // Close socket
        socket_close($socket);

        $lines = explode("\n", $buf);
        foreach ($lines as $line)
        {
            $dbg->info("MAC" . $idx . "(" . strlen($line) . "): " . $line );
            if ( strlen($line) == 17 )
            {
                $page = str_replace("[MAC".$idx."]", $line, $page);
                $idx++;
                if ( $idx == 9 )
                    break;
            }
        }
    }

    // Clear any unused fields
    for($i=$idx; $i<9; $i++)
    {
        $page = str_replace("[MAC".$idx++."]", "", $page);
    }
    $dbg->info("MAC table\n" . $page );
    print $page;
}

/*
 * ---------------------------------------------------------------
 * Retrieve the current macaddr_acl setting
 * ---------------------------------------------------------------
 */
function getACLState()
{
    global $dbg;
    $tmpl = "/etc/hostapd.conf";
    $dbg->info("Reading template " . $tmpl);
    $file = file_get_contents($tmpl);
    $lines = explode("\n", $file);
    $rc = "0";
    foreach ($lines as $line)
    {
        if ( strlen( trim($line) ) == 0 )
            continue;
        $fields = explode("=", $line);
        if ( strcasecmp($fields[0], "macaddr_acl") == 0 )
        {
            $rc = $fields[1];
            break;
        }
    }
    $dbg->info("Current state: " . $rc);
    return $rc;
}

/*
 * ---------------------------------------------------------------
 * Build a series of text input fields for static IP configurations.
 * ---------------------------------------------------------------
 */
function buildStaticFields($config)
{
    $html = "<table>\n";

    $html .= "<tr>\n";
    $html .= "\t<td> IP Address </td>\n";

    if ( in_array("ADDRESS", $config) ) { $value = "value=\"" . $config["ADDRESS"] . "\""; }
    else                                { $value = ""; }
    if ( in_array("NETMASK", $config) ) { $value = "value=\"" . $config["NETMASK"] . "\""; }
    else                                { $value = ""; }
    if ( in_array("GATEWAY", $config) ) { $value = "value=\"" . $config["GATEWAY"] . "\""; }
    else                                { $value = ""; }

    $html .= "<td> <input type=\"text\" name=\"ipaddress\"" . $value . " > </td>\n";
    $html .= "</tr>\n";
    
    $html .= "</table>\n";
    return $html;
}

/*
 * ---------------------------------------------------------------
 * Read static network configuration (IP addr, Gateway, etc.)
 * ---------------------------------------------------------------
 */
function getIFStatic( $interface )
{
    // Error check the interface name.
    if ( $interface == "" )
    {
        $config['NOCONFIG'] = "";
        return $config;
    }

    // Retrieve first interface configuration
    $socket = getSocket(1);
    $header = 0x00000203;   // MT_NET, MA_GETIF
    $size = strlen($interface);
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $interface, strlen($interface));
    usleep(20);

    // Read response
    // Format of response:
    // newline-separated list of config options, as name:value pairs.
    if ( false === ($buf=socket_read($socket, 2048, PHP_NORMAL_READ)) )
    {
        error_log("Failed reading response to MS_GETIF.");
        return -1;
    }

    // Close socket
    socket_close($socket);

    // Parse buffer
    $lines = explode("\n", $buf);
    $config = array();
    foreach ($lines as $line)
    {
        if ( strlen( trim($line) ) == 0 )
            continue;
        $fields = explode(":", $line);
        $config[ $fields[0] ] = $fields[1];
    }

    return $config;
}

/*
 * ---------------------------------------------------------------
 * Read DNS configuration
 * ---------------------------------------------------------------
 */
function getDNS()
{
    global $dbg;

    $socket = getSocket(1);
    $header = 0x00000303;   // MT_NET, MA_GETDNS
    socket_write($socket, pack("I", $header), 4);
    usleep(20);

    // Read response
    // Format of response:
    // space-separated list of addresses.
    if ( false === ($buf=socket_read($socket, 2048, PHP_NORMAL_READ)) )
    {
        $dbg->info("Failed reading response to MS_GETDNS.");
        return -1;
    }

    // Close socket
    socket_close($socket);

    // Parse buffer
    $buf = trim($buf);
    $addresses = explode(" ", $buf);
    return $addresses;
}

/*
 * ---------------------------------------------------------------
 * Read Wireless client configuration (wpa_supplicant.conf)
 * ---------------------------------------------------------------
 */
function getWireless()
{
    global $dbg;

    $socket = getSocket(1);
    $header = 0x00000603;   // MT_NET, MA_GETWIRELESS
    socket_write($socket, pack("I", $header), 4);
    usleep(20);

    // Read response
    // Format of response:
    // colon seperated list of values, in the following order:
    // ssid
    // security type
    // password
    if ( false === ($buf=socket_read($socket, 2048, PHP_NORMAL_READ)) )
    {
        $dbg->info("Failed reading response to MS_GETWIRELESS.");
        return -1;
    }

    // Close socket
    socket_close($socket);

    // Parse buffer
    $buf = trim($buf);
    $fields = explode(":", $buf);
    return $fields;
}

/*
 * ---------------------------------------------------------------
 * Read host access point configuration 
 * ---------------------------------------------------------------
 */
function getAP()
{
    global $dbg;
    $socket = getSocket(1);
    $header = 0x00000503;   // MT_NET, MA_GETAP
    socket_write($socket, pack("I", $header), 4);
    usleep(20);

    // Read response
    // Format of response:
    // colon seperated list of values, in the following order:
    // ssid
    // channel
    // password
    if ( false === ($buf=socket_read($socket, 2048, PHP_NORMAL_READ)) )
    {
        $dbg->info("Failed reading response to MS_GETAP.");
        return -1;
    }

    // Close socket
    socket_close($socket);

    // Parse buffer
    $buf = trim($buf);
    $fields = explode(":", $buf);
    return $fields;
}

/*
 * ---------------------------------------------------------------
 * Request interface MAC address
 * ---------------------------------------------------------------
 */
function getMAC( $interface )
{
    // Error check the interface name.
    if ( $interface == "" )
    {
        $config['NOCONFIG'] = "";
        return "";
    }

    // Retrieve first interface configuration
    $socket = getSocket(1);
    $header = 0x00000B03;   // MT_NET, MA_GETMAC
    $size = strlen($interface);
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $interface, strlen($interface));
    usleep(20);

    // Read response
    // Format of response:
    // Newline terminated line of data
    if ( false === ($buf=socket_read($socket, 2048, PHP_NORMAL_READ)) )
    {
        error_log("Failed reading response to MA_GETMAC.");
        return -1;
    }

    // Close socket
    socket_close($socket);

    $lines = explode("\n", $buf);
    return $lines[0];
}

/*
 * ---------------------------------------------------------------
 * Request interface IP address
 * ---------------------------------------------------------------
 */
function getIP( $interface )
{
    // Error check the interface name.
    if ( $interface == "" )
    {
        $config['NOCONFIG'] = "";
        return "";
    }

    // Retrieve first interface configuration
    $socket = getSocket(1);
    $header = 0x00000C03;   // MT_NET, MA_GETIP
    $size = strlen($interface);
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $interface, strlen($interface));
    usleep(20);

    // Read response
    // Format of response:
    // Newline terminated line of data
    if ( false === ($buf=socket_read($socket, 2048, PHP_NORMAL_READ)) )
    {
        error_log("Failed reading response to MA_GETMAC.");
        return -1;
    }

    // Close socket
    socket_close($socket);

    $lines = explode("\n", $buf);
    return $lines[0];
}

/*
 * ---------------------------------------------------------------
 * IPV4 tab: configure DHCP/Static and (if necessary) ip addresses.
 * ---------------------------------------------------------------
 */
function settingsIPV4()
{
    global $dbg;
    $dbg->info("Entered settingsIPV4");

    // Setup connection to piboxd.
    $socket = getSocket(1);

    // Retrieve interface names
    $header = 0x00000103;   // MT_NET, MA_GETIFLIST

    // Header needs to be pack()'d
    socket_write($socket, pack("I", $header), 4);
    usleep(20);

    // Read response
    // Format of response:
    // Space-separated list of interface names
    if ( false === ($buf=socket_read($socket, 2048, PHP_NORMAL_READ)) )
    {
        error_log("Failed reading response to MS_GETIFLIST.");
        return -1;
    }
    $interfaces = explode(" ", trim($buf));

    // Choose which interface should be the one displayed.
    if ( isset($_GET['interface']) )
    {
        $activeIF = $_GET["interface"];
    }
    else
    {
        $activeIF = $interfaces[0];
    }
    $dbg->info("Getting config for: " . $activeIF);

    // Build options menu from list of interface names
    $ifHTML = "<select id=\"Interfaces\" onchange=\"ifChanged(this.value)\">\n";
    $selected = 0;
    foreach ($interfaces as $interface)
    {
        if ( strlen( trim($interface) ) == 0 )
            continue;
        if ( $interface == $activeIF )
            $selected = 1;

        if ( $selected == 1 )
        {
            $ifHTML .= "<option value=\"" . $interface . "\" selected> " . $interface . " </option>\n";
            $selected = 0;
        }
        else
            $ifHTML .= "<option value=\"" . $interface . "\"> " . $interface . " </option>";
    }
    $ifHTML .= "</select>\n";
    $dbg->info("Select string:" . $ifHTML);

    // Close socket
    socket_close($socket);

    // Retrieve first interface configuration
    $config = getIFStatic( $activeIF );
    $dbg->info("config: " . print_r($config, true));

    // Build page from appropriate template.
    $tmpl = "settings.tmpl";
    $page = file_get_contents($tmpl);
    $page = str_replace("[INTERFACE]", $ifHTML, $page);

    $addrTypeHTML = "<select id=\"AddressType\" onchange=\"addressTypeChanged(this)\">\n";
    if ( !isset($config['NOCONFIG']) )
    {
        $dbg->info($activeIF . " has a configuration");

        // Find address type, if defined.
        $dhcpSelected = "";
        $staticSelected = "";
        if ( in_array("ADDRESSTYPE", $config) )
        {
            if ( strcasecmp($config["ADDRESSTYPE"], "DHCP" ) )
            {
                $dhcpSelected = "selected";
                $staticFieldsHTML = "";
            }
            else if ( strcasecmp($config["ADDRESSTYPE"], "Static" ) )
            {
                $staticSelected = "selected";
                $staticFieldsHTML = buildStaticFields($config);
            }
        }
        $addrTypeHTML .= "<option value=\"DHCP\" "   . $dhcpSelected   . "> DHCP </option>\n";
        $addrTypeHTML .= "<option value=\"Static\" " . $staticSelected . "> Static </option>\n";
        $page = str_replace("[CHECKEDY]", "checked", $page);
        $page = str_replace("[CHECKEDN]", "", $page);
    }
    else
    {
        $dbg->info($activeIF . " has no configuration");

        $addrTypeHTML .= "<option value=\"DHCP\" selected > DHCP </option>\n";
        $addrTypeHTML .= "<option value=\"Static\" > Static </option>\n";
        $page = str_replace("[CHECKEDN]", "checked", $page);
        $page = str_replace("[CHECKEDY]", "", $page);
    }
    $addrTypeHTML .= "</select>\n";
    $page = str_replace("[ADDRESSTYPE]", $addrTypeHTML, $page);

    // MAC Address
    $macAddress = getMAC($activeIF);
    $page = str_replace("[MACADDRESS]", $macAddress, $page);

    // IP Address
    $ipAddress = getIP($activeIF);
    $page = str_replace("[IPADDRESS]", $ipAddress, $page);

    // Print page.
    print $page;
    return 0;
}

/*
 * ---------------------------------------------------------------
 * Retrieve the list of static fields for the specified interface.
 * Returns empty HTML if none are found.
 * ---------------------------------------------------------------
 */
function getStaticFields()
{
    if ( !isset($_GET['interface']) )
    {
        print "";
        return;
    }

    // Build and send request to daemon.
    $config = getIFStatic ( $_GET['interface'] );
    $dnsConfig = getDNS ();

    // If no configuration, return empty string.
    if ( in_array("NOCONFIG", $config) )
    {
        print "";
        return;
    }

    // Build a table of text fields
    $html  = "<table>\n";

    // IP Address
    $html .= "<tr>\n";
    $html .= "    <td> IP Address </td>\n";
    $html .= "    <td> ";
    if ( in_array("ADDRESS", $config) ) { $value = "value=\"". $config["ADDRESS"] . "\""; }
    else                                { $value = ""; }
    $html .= "<input type=\"text\" id=\"IPAddress\"" . $value . ">";
    $html .= "    </td>\n";
    $html .= "<tr>\n";

    // Netmask
    $html .= "<tr>\n";
    $html .= "    <td> Netmask </td>\n";
    $html .= "    <td>";
    if ( in_array("NETMASK", $config) ) { $value = "value=\"". $config["NETMASK"] . "\""; }
    else                                { $value = ""; }
    $html .= "<input type=\"text\" id=\"Netmask\"" . $value . ">";
    $html .= "    </td>\n";
    $html .= "<tr>\n";

    // Gateway
    $html .= "<tr>\n";
    $html .= "    <td> Gateway </td>\n";
    $html .= "    <td>";
    if ( in_array("GATEWAY", $config) ) { $value = "value=\"". $config["GATEWAY"] . "\""; }
    else                                { $value = ""; }
    $html .= "<input type=\"text\" id=\"Gateway\"" . $value . ">";
    $html .= "    </td>\n";
    $html .= "<tr>\n";

    // DNS
    for ($i=0; $i<3; $i++)
    {
        $html .= "<tr>\n";
        $html .= "    <td> DNS " . $i . " </td>\n";
        $html .= "    <td>";
        if ( count($dnsConfig)>$i ) { $value = "value=\"". $dnsConfig[$i] . "\""; }
        else                        { $value = ""; }
        $html .= "<input type=\"text\" id=\"DNS" . $i . "\"" . $value . ">";
        $html .= "    </td>\n";
        $html .= "<tr>\n";
    }

    $html .= "</table>\n";
    print $html;
}

/*
 * ---------------------------------------------------------------
 * Retrieve current network mode (wireless client or access point)
 * ---------------------------------------------------------------
 */
function getMode()
{
    global $dbg;

    $socket = getSocket(1);
    $header = 0x00000403;   // MT_NET, MA_GETNETTYPE
    socket_write($socket, pack("I", $header), 4);
    usleep(20);

    // Read response
    // Format of response:
    // space-separated list of addresses.
    if ( false === ($buf=socket_read($socket, 2048, PHP_NORMAL_READ)) )
    {
        $dbg->info("Failed reading response to MS_GETNETTYPE.");
        print "getMode failed: failed reading MS_GETNETTYPE.";
        return;
    }

    // Close socket
    socket_close($socket);

    // Cleanup and return buffer
    $buf = trim($buf);
    $dbg->info("Current network mode: " . $buf);
    print $buf;
}

/*
 * ---------------------------------------------------------------
 * Save current network mode (wireless client or access point)
 * ---------------------------------------------------------------
 */
function setMode()
{
    global $dbg;

    if ( empty($_POST) )
    {
        $dbg->info("setMode: no POST data");
        return;
    }
    $dbg->info("setMode POST data: " . print_r($_POST, true));

    // Find the interface we're updating.
    if ( isset($_POST['mode']) ) 
    { 
        $mode = $_POST['mode']; 
    }
    else
    {
        $dbg->info("setMode: no MODE specified - ignoring set request.");
        print "Save failed: missing mode.";
        return;
    }

    $socket = getSocket(1);
    $header = 0x00000a03;   // MT_NET, MA_SETNETTYPE
    $size = strlen($mode);
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $mode, strlen($mode));
    usleep(20);

    // Close socket
    socket_close($socket);
}

/*
 * ---------------------------------------------------------------
 * Save data from the UI
 * ---------------------------------------------------------------
 */
function save()
{
    global $dbg;
    $dbg->info("save: Entered");

    if ( empty($_POST) )
    {
        $dbg->info("save: no POST data");
        return;
    }
    $dbg->info("save POST data: " . print_r($_POST, true));

    // The "type" determines what we're saving.
    if ( !isset($_POST['type']) ) 
    { 
        $dbg->info("save: missing TYPE in POST data");
        return;
    }
    $type = strtolower( $_POST['type'] );
    if      ( $type == "ipv4"        ) { saveIPV4(); }
    else if ( $type == "wireless"    ) { saveWireless(); }
    else if ( $type == "accesspoint" ) { saveAccessPoint(); }
    else $dbg->info("save: invalid TYPE in POST data" . $type);
}

// Save an interface configuration.
function saveIPV4()
{
    global $dbg;
    $dbg->info("saveIPV4: Entered");

    // Find the interface we're updating.
    if ( isset($_POST['interface']) ) 
    { 
        $iface = $_POST['interface']; 
    }
    else
    {
        $dbg->info("saveIPV4: no INTERFACE specified - ignoring save request.");
        print "Save failed: missing interface.";
        return;
    }

    // Are we enabled?
    if ( isset($_POST['enabled']) ) 
    { 
        $estate = $_POST['enabled']; 
    }
    else
    {
        $dbg->info("saveIPV4: failed: missing enabled state");
        print "Save failed: missing enabled state.";
        return;
    }

    // What kind of configuration are we using.
    if ( isset($_POST['config']) ) 
    { 
        $ctype = $_POST['config']; 
    }
    else
    {
        $dbg->info("saveIPV4: failed: missing config type");
        print "Save failed: missing config type.";
        return;
    }

    // ipCheck will set address to -1 if address is invalid.
    $dbg->info("Testing ipaddr");
    if ( isset($_POST['ipaddr'])    ) { $ipaddr  = ipCheck( $_POST['ipaddr'] ); }
    else                              { $ipaddr  = "-1"; }
    $dbg->info("Testing netmask");
    if ( isset($_POST['netmask'])   ) { $netmask = ipCheck( $_POST['netmask'] ); }
    else                              { $netmask = "-1"; }
    $dbg->info("Testing gateway");
    if ( isset($_POST['gateway'])   ) { $gateway = ipCheck( $_POST['gateway'] ); }
    else                              { $gateway = "-1"; }
    $dbg->info("Testing dns0");
    if ( isset($_POST['dns0'])      ) { $dns0    = ipCheck( $_POST['dns0'] ); }
    else                              { $dns0    = "-1"; }
    $dbg->info("Testing dns1");
    if ( isset($_POST['dns1'])      ) { $dns1    = ipCheck( $_POST['dns1'] ); }
    else                              { $dns1    = "-1"; }
    $dbg->info("Testing dns2");
    if ( isset($_POST['dns2'])      ) { $dns2    = ipCheck( $_POST['dns2'] ); }
    else                              { $dns2    = "-1"; }

    /*
     * Build the payload for the message.
     * If a field is not used or unavailable it is set to -1.
     * Order of fields for the payload:
     * 1. Interface (name of interface)
     * 2. Enabled state (yes/no)
     * 3. Configuration type (DHCP/Static)
     * 4. IP Address 
     * 5. Netmask
     * 6. Gateway
     * 7. DNS 0
     * 8. DNS 1
     * 9. DNS 2
     */
    $dbg->info("Building message");
    $msg = $iface . ":" . $estate . ":" . $ctype . ":" . $ipaddr . ":" . $netmask . ":" . 
           $gateway . ":" . $dns0 . ":" . $dns1 . ":" . $dns2;

    // Build a packet to send to piboxd
    // We don't wait for a response.
    $dbg->info("Sending message to piboxd");
    $socket = getSocket(1);
    $header = 0x00000703;   // MT_NET, MA_SETIPV4
    $size = strlen($msg);
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $msg, strlen($msg));

    $dbg->info("saveIPV4: done.");
    print "Configuration saved.";
}

// Save wireless client configuration.
function saveWireless()
{
    global $dbg;
    $dbg->info("saveWireless: Entered");

    $ssid = $_POST['ssid'];
    $security = $_POST['security'];
    $pw = $_POST['pw'];
    $msg = $ssid . ":" . $security . ":" . $pw;

    $dbg->info("Sending message to piboxd");
    $socket = getSocket(1);
    $header = 0x00000903;   // MT_NET, MA_SETWIRELESS
    $size = strlen($msg);
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $msg, strlen($msg));

    $dbg->info("saveWireless: done.");
    print "Configuration saved.";
}

// Save access point configuration
function saveAccessPoint()
{
    global $dbg;
    $dbg->info("saveAccessPoint: Entered");

    if ( isset($_POST['usemac']) )
        $usemac = 1;
    else
        $usemac = 0;

    $ssid = $_POST['ssid'];
    $channel = $_POST['channel'];
    $pw = $_POST['pw'];
    $apbase = $_POST['base'];
    $msg = $ssid . ":" . $channel . ":" . $pw . ":" . $apbase;

    // Save access point configuration information
    $dbg->info("Sending MT_NET/MA_SETAP message to piboxd");
    $socket = getSocket(1);
    if ($socket === false)
    {   
        print "Failed to connect to monitor.\n";
        return;
    }
    $header = 0x00000803;   // MT_NET, MA_SETAP
    $size = strlen($msg);
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $msg, strlen($msg));
    socket_close($socket);

    // If MAC filtering is enabled, save the mac addresses
    if ( isset($_POST['usemac']) )
    {
        $dbg->info("Enabling MAC filtering");

        // Build mac address list
        $macAddrs = "";
        for($i=1; $i<9; $i++)
        {
            if ( isset($_POST['mac'.$i]) )
            {
                // Validate addresses first
                $addr = $_POST['mac'.$i];
                if ( !preg_match('/^([0-9a-fA-F]{2}[:]){5}([0-9a-fA-F]{2})$/', $addr) )
                {
                    // Ignore bad addresses
                    $dbg->info("mac".$i." has a bad address: " . $addr);
                    continue;
                }
                $macAddrs = $macAddrs . $addr . "\n";
            }
            else
                $dbg->info("mac".$i." is not in the POST");
        }

        // If we have at least one address then we can enable mac filtering.
        $dbg->info( "macAddrs(".strlen($macAddrs)."): " . $macAddrs);
        if ( strlen($macAddrs) > 17 )
        {
            // Tell piboxd to update hostapd.accept.
            $dbg->info("Sending MT_NET/MA_MFLIST message to piboxd");
            $socket = getSocket(1);
            if ($socket === false)
            {   
                print "Failed to connect to monitor.\n";
                if ( $doreturn )
                    return 1;
                return;
            }
            $header = 0x00020D03;   // MT_NET, MA_MFLIST, Set
            $size = strlen($macAddrs);
            socket_write($socket, pack("I", $header), 4);
            socket_write($socket, pack("I", $size), 4);
            socket_write($socket, $macAddrs, strlen($macAddrs));
            socket_close($socket);
        }
        else
            $dbg->info("No MAC addresses found.");
    }

    $dbg->info("saveAccessPoint: done.");
    print "Configuration saved.";
}

/*
 * ---------------------------------------------------------------
 * Access Point tab: configure RPi as an access point.
 * ---------------------------------------------------------------
 */
function settingsAccessPoint()
{
    // Get wireless client settings from piboxd
    $config = getAP();

    // Build and print HTML page
    $html  = "<table>\n";

    // SSID
    $html .= "<tr>\n";
    $html .= "    <td> SSID </td>\n";
    $html .= "    <td> ";
    if ( count($config)>0 ) { $value = "value=\"". $config[0] . "\""; }
    else                    { $value = ""; }
    $html .= "<input type=\"text\" id=\"APSSID\"" . $value . ">";
    $html .= "    </td>\n";
    $html .= "</tr>\n";

    // Channel type
    $channels = array ( "1", "5", "9", "13" );

    $html .= "<tr>\n";
    $html .= "    <td> Channel </td>\n";
    $html .= "    <td>";
    $html .= "<select id=\"APCH\" >\n";
    if ( count($config)>1 ) { $value = $config[1]; }
    else                    { $value = ""; }
    foreach($channels as $ch)
    {
        $selected = "";
        if ( ($value == "") && ($ch == "1") ) { $selected = "selected"; }
        else if ( $ch == $value ) { $selected = "selected"; }
        $html .= "<option value=\"" . $ch . "\" " . $selected . ">" . $ch . "</option>\n";
    }
    $html .= "</select>\n";
    $html .= "    </td>\n";
    $html .= "</tr>\n";

    // Base Address
    $html .= "<tr>\n";
    $html .= "    <td> Base Address </td>\n";
    $html .= "    <td>";
    if ( count($config)>3 ) { $value = "value=\"". $config[3] . "\""; }
    else                    { $value = ""; }
    $html .= "<input type=\"text\" id=\"APBASE\" " . $value . " \"/>";
    $html .= "    </td>\n";
    $html .= "</tr>\n";

    // Password
    $html .= "<tr>\n";
    $html .= "    <td> Password </td>\n";
    $html .= "    <td>";
    if ( count($config)>2 ) { $value = "value=\"". $config[2] . "\""; }
    else                    { $value = ""; }
    $html .= "<input type=\"password\" id=\"APPW\"" . $value . " onchange=\"appwChanged('v')\"/>";
    $html .= "<input type=\"text\" id=\"APPWhidden\"" . $value . " style='display:none;' ".
                "onchange=\"appwChanged('h')\"/>";
    $html .= "    </td>\n";
    $html .= "</tr>\n";

    // Show/Hide password button
    $html .= "<tr>\n";
    $html .= "    <td colspan=\"2\" > ";
    $html .= "    <label class=\"ShowHide\" > ";
    $html .= "<input type=\"checkbox\" id=\"APPWshow\" onclick=\"appwShow()\"/>Show Password";
    $html .= "    </label> ";
    $html .= "    </td>\n";
    $html .= "</tr>\n";

    // Enable/Disable MAC Filtering (re: Show/Hide mac filtering input fields).
    $html .= "<tr>\n";
    $html .= "    <td colspan=\"2\" > ";
    $html .= "    <label class=\"ShowHide\" > ";
    $html .= "<input type=\"checkbox\" id=\"MACshow\" onclick=\"macShow()\"/>MAC Filtering";
    $html .= "    </label> ";
    $html .= "    </td>\n";
    $html .= "</tr>\n";
    $html .= "</table>\n";

    // MAC address input fields (dynamically displayed)
    $html .= "<span id=\"macFields\"> </span>\n";

    print $html;
}

/*
 * ---------------------------------------------------------------
 * Wireless client tab: configure RPi as a wireless client.
 * ---------------------------------------------------------------
 */
function settingsWireless()
{
    // Get wireless client settings from piboxd
    $config = getWireless();

    // Build and print HTML page
    $html  = "<table>\n";

    // SSID
    $html .= "<tr>\n";
    $html .= "    <td> SSID </td>\n";
    $html .= "    <td> ";
    if ( count($config)>0 ) { $value = "value=\"". $config[0] . "\""; }
    else                    { $value = ""; }
    $html .= "<input type=\"text\" id=\"WCSSID\"" . $value . ">";
    $html .= "    </td>\n";
    $html .= "</tr>\n";

    // Security type
    $securityType = array ( "WPA & WPA2 Personal", "WPA & WPA2 Enterprise" );

    $html .= "<tr>\n";
    $html .= "    <td> Security </td>\n";
    $html .= "    <td>";
    $html .= "<select id=\"WCSEC\" >\n";
    if ( count($config)>1 ) { $value = $config[1]; }
    else                    { $value = ""; }
    foreach($securityType as $sec)
    {
        $selected = "";
        if ( ($value == "") && ($sec == "1") ) { $selected = "selected"; }
        else if ( $sec == $value ) { $selected = "selected"; }
        $html .= "<option value=\"" . $sec . "\" " . $selected . ">" . $sec . "</option>\n";
    }
    $html .= "</select>\n";
    $html .= "    </td>\n";
    $html .= "</tr>\n";

    // Password
    $html .= "<tr>\n";
    $html .= "    <td> Password </td>\n";
    $html .= "    <td>";
    if ( count($config)>2 ) { $value = "value=\"". $config[2] . "\""; }
    else                    { $value = ""; }
    $html .= "<input type=\"password\" id=\"WCPW\"" . $value . " onchange=\"wcpwChanged('v')\"/>";
    $html .= "<input type=\"text\" id=\"WCPWhidden\"" . $value . 
                " style='display:none;' onchange=\"wcpwChanged('h')\"/>";
    $html .= "    </td>\n";
    $html .= "</tr>\n";

    // Show/Hide password button
    $html .= "<tr>\n";
    $html .= "    <td colspan=\"2\" > ";
    $html .= "    <label class=\"ShowHide\" > ";
    $html .= "<input type=\"checkbox\" id=\"WCPWshow\" onclick=\"wcpwShow()\"/>Show Password";
    $html .= "    </label> ";
    $html .= "    </td>\n";
    $html .= "</tr>\n";

    $html .= "</table>\n";
    print $html;
}

/*
 * ---------------------------------------------------------------
 * Handle configuration requests of the piboxd daemon
 * and the web interface.
 * ---------------------------------------------------------------
 */
function settings()
{
    global $dbg;

    // Subpages are handled first.  GETs return HTML snippets.
    if ( isset($_GET['subPage']) )
    {
        $subPage = $_GET['subPage'];
        $dbg->info("Subpage is set: " . $subPage);
        if ( strcmp($subPage, "staticFields") == 0 ) { getStaticFields(); }
        else if ( strcmp($subPage, "getMode") == 0 ) { getMode(); }
        return;
    }
    // POSTs update the server, and may return HTML snippets.
    if ( isset($_POST['subPage']) )
    {
        $subPage = $_POST['subPage'];
        $dbg->info("Subpage is set: " . $subPage);
        if ( strcmp($subPage, "save") == 0 ) { save(); }
        else if ( strcmp($subPage, "setMode") == 0 ) { setMode(); }
        return;
    }

    // Retrieve MAC ACL state
    if ( isset($_GET['getACLState']) )
    {
        $dbg->info("Getting ACL state.");
        print getACLState();
        return;
    }

    // Tabs build the specific tab content and return a complete HTML page.
    if ( !isset($_GET['tab']) )
    {
        $dbg->info("Missing tab field.");
        header( 'Location: /index.html' ) ;
        return;
    }

    $tab = $_GET['tab'];
    $dbg->info("Processing tab: " . $tab);
    $rc = 0;
    if      ( $tab == "ipv4" )        { $rc = settingsIPV4(); }
    else if ( $tab == "accessPoint" ) { $rc = settingsAccessPoint(); }
    else if ( $tab == "wireless" )     { $rc = settingsWireless(); }
    else 
        $rc = -1;

    if ( $rc == -1 )
        header( 'Location: /index.html' ) ;
}
    
?>
