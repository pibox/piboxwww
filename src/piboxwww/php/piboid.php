<?php

/*
 * ---------------------------------------------------------------
 * piboid.php
 * Android-based remote control of PiBox.
 * Called only from device.php.
 * ---------------------------------------------------------------
 */

function piboid($api)
{
    global $dbg;

    // Make sure we got all three fields
    if ( count($api) != 4 )
    {
        $dbg->info("piboid: Invalid RESTful format: field count = " . count($api));
        return;
    }

    // We don't have multiple versions yet but this is where we'd
    // setup for that if we did...

    // Get a socket to the appmgr
    // Socket creator logs error messages.
    $socket = getAppMgrSocket();
    if ( $socket === false )
        return;

    // Send payload to appmgr, who will forward it to the currently
    // running app, if any.
    $header = 0x00000003;   // MT_KEY (see appmgr.h)
    $size = strlen($interface);
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $api[3], strlen($api[3]));
    socket_close($socket);
}

?>
