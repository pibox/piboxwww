<?php

/*
 * ---------------------------------------------------------------
 * Display the front page.  Update the default pw if necessary.
 * ---------------------------------------------------------------
 */
function frontPage()
{
    global $dbg;

    // Test if default pw has been changed.
    if ( updatePW() != "" )
        return;

    $remoteIP = $_SERVER['REMOTE_ADDR'];
    $myIP = getMyIP();
    $dbg->info("Remote IP: ". $remoteIP);
    $dbg->info("MY IP    : ". $myIP);

    // Everyone can access the webcam.
    $html  = "<ul class=\"mainMenu\">\n";
    $html .= "   <li> <a id=\"webcamIcon\" href=\"/php/pibox.php?function=webcam&res=low\">Webcam</a> </li>\n";

    // And the network settings
    $html .= "   <li> <a id=\"settingsIcon\" ";
    $html .= "href=\"/php/pibox.php?function=settings&tab=ipv4\">Settings</a> </li>\n";

    // And the user users
    $html .= "   <li> <a id=\"usersIcon\" ";
    $html .= "href=\"/php/pibox.php?function=users\">Users</a> </li>\n";

    $html .= "</ul>\n";
    $dbg->info("HTML: ". $html);
    print $html;
}

/*
 * ---------------------------------------------------------------
 * Display the front page.  Update the default pw if necessary.
 * ---------------------------------------------------------------
 */
function updatePW()
{
    global $dbg;

    // The stamp file gets created by piboxd when we submit this change.
    $stamp = "/etc/lighttpd/DEFAULT_CHANGED";

    // Test if we've updated the default pw
    if ( file_exists($stamp) )
        return "";

    // Read the user PW page template
    $tmpl = "userPW.tmpl";
    $page = file_get_contents($tmpl);
    $page = str_replace("[USERNAME]", "admin", $page);

    // Display the page.
    print $page;
    return "1";
}
    
?>
