<?php

/*
 * ---------------------------------------------------------------
 * Display a list of users and allow selection of one or add a
 * new user.
 * ---------------------------------------------------------------
 */
function getUsers()
{
    global $dbg;

    // Build request to get list of users
    $socket = getSocket(1);
    $header = 0x00000204;   // MT_PW, MA_GET
    socket_write($socket, pack("I", $header), 4);
    usleep(20);

    // Read response
    // Format of response:
    // newline-separated list of usernames.
    if ( false === ($buf=socket_read($socket, 2048)) )
    {
        $dbg->info("Failed reading response to MT_PW, MA_GET.");
        return -1;
    }

    // Close socket
    socket_close($socket);

    // Parse buffer
    // $buf = trim($buf);
    $dbg->info("Users:\n" . $buf);
    $usernames = explode("\n", $buf);
    return $usernames;
}

/*
 * ---------------------------------------------------------------
 * Display a list of users and allow selection of one or add a
 * new user.
 * ---------------------------------------------------------------
 */
function users()
{
    global $dbg;

    $tmpl = "users.tmpl";
    $dbg->info("Reading template " . $tmpl);
    $page = file_get_contents($tmpl);

    // Retrieve user names
    $dbg->info("Getting usernames.");
    $usernames = getUsers();

    // Build a table of usernames to edit
    $dbg->info("Building table.");
    $html  = "<table align=\"center\" cellspacing=\"10\">\n";
    $html .= "    <tr>\n";
    $html .= "        <th colspan=\"2\"> Select a user to update, create a new user or delete an existing user </th>\n";
    $html .= "    </tr>\n";
    foreach ($usernames as $username)
    {
        $username = trim($username);
        if (strlen($username) == 0 ) 
            continue;

        $html .= "    <tr>\n";

        // Admin user cannot be deleted.
        if (strcasecmp($username, 'admin') == 0 ) 
        {
            $html .= "        <td> </td>\n";
        }
        else
        {
            $html .= "        <td> <button class=\"navButton\" id=\"delete\" name=\"delete\" " .
                                   "onclick=\"deleteUser('" . $username . "')\">x</input></td>\n";
        }

        $html .= "        <td> <span id=\"username\" onclick=\"editUser('" . $username . "')\" " .
                                "onmouseover=\"this.style.cursor='pointer'\" " .
                                "onmouseout=\"this.style.cursor='default'\">" . $username . "</span></td>\n";
        $html .= "    </tr>\n";
    }
    $html .= "    <tr>\n";
    $html .= "        <td> <button class=\"navButton\" id=\"add\" name=\"add\" onclick=\"addUser()\">Add</input></td>\n";
    $html .= "        <td> <input type=\"text\" id=\"newuser\"> </td>\n";
    $html .= "    </tr>\n";
    $html .= "    <tr>\n";
    $html .= "        <td align=\"center\" colspan=\"2\"> <span id=\"msgBox\"></span> </td>\n";
    $html .= "    </tr>\n";
    $html .= "</table>\n";
    $page = str_replace("[USERS]", $html, $page);
    print $page;
}

/*
 * ---------------------------------------------------------------
 * Display the userPW page when called from the users page.
 * Works for either a user update or adding a new user.
 * ---------------------------------------------------------------
 */
function updateUser()
{
    global $dbg;

    // Retrieve username
    if ( isset($_POST['username']) ) 
    { 
        $username = $_POST['username']; 
        $dbg->info("updateUser: username = " . $username);
    }
    else
    {
        $dbg->info("updateUser: no username specified - ignoring request.");
        header( 'Location: /php/pibox.php?function=users' ) ;        
        return;
    }

    // Display password change content
    if ( !isset($_POST['delete']) ) 
    { 
        $dbg->info("updateUser: request to update.");

        // The template of the pw fields
        $tmpl = "userPW.tmpl";
        $dbg->info("Reading template " . $tmpl);
        $html = file_get_contents($tmpl);

        // String replacements, then return the data.
        $html = str_replace("[USERNAME]", $username, $html);
        $dbg->info("updateUser: html = \n" . $html);
        print $html;
    }
    else
    {
        $dbg->info("updateUser: request to delete.");
        // Tell piboxd to delete the user, the return to the caller who will
        // redirect after a short delay.
        $socket = getSocket(1);
        $header = 0x00000304;   // MT_PW, MA_DEL
        $size = strlen(username);
        socket_write($socket, pack("I", $header), 4);
        socket_write($socket, pack("I", $size), 4);
        socket_write($socket, $username, strlen($username));
        usleep(20);
        socket_close($socket);
        print "1";
    }

}

?>
