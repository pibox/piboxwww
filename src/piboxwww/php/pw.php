<?php

/*
 * ---------------------------------------------------------------
 * Grab username/userpw from inbound data and pass it to
 * the piboxd daemon to update the user db.  It will then restart
 * the web server for us.
 * ---------------------------------------------------------------
 */
function savepw()
{
    // Get the username and pw
    $username=$_POST["username"];
    $pw=$_POST["pw"];
    $payload = $username . ":" . $pw;

    // Build pw request
    $header = 0x00000104; // MT_PW, MA_SAVE
    $size = strlen($payload);

    // Setup connection to piboxd.
    $socket = getSocket();

    // Send request to piboxd.
    // Binary data needs to be pack()'d, but not sting data.
    // Order here is important!
    socket_write($socket, pack("I", $header), 4);
    socket_write($socket, pack("I", $size), 4);
    socket_write($socket, $payload, $size);
    socket_close($socket);

    // Print results.
    print "ok";
}

?>
