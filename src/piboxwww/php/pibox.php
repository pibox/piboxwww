<?php

/*
 * ---------------------------------------------------------------
 * PiBox routing.
 * This file contains the tests required to determine what
 * functions should be called for the UI.
 *
 * It does not contain support for any of the REST API.
 * See devices.php for the REST API.
 * ---------------------------------------------------------------
 */

/* Initialization */
include "init.php";
$iminit = IMInit::Instance();
$iminit->setEnv();

ini_set('display_errors', 'On');
ini_set('error_log', $iminit->get("logpath") . '/piboxwww.err');
error_reporting(E_NOTICE);

error_log("Web Server path: " . $iminit->get("wspath"));
error_log("Log path: " . $iminit->get("logpath"));

/*
 * Debugging and other globally useful classes and functions.
 */
include "util.php";
$dbg = new PiboxLog();
// Uncomment this to enable debugging
// $dbg->setLevel('DEBUG');

// Front page handler.
include "frontpage.php";

// Webcam handler
include "webcam.php";

// Settings handler.
include "settings.php";

// Password manager
include "pw.php";

// User manager
include "users.php";

/*
 * ---------------------------------------------------------------
 * Main routine - test function API
 * ---------------------------------------------------------------
 */
function main()
{
    global $dbg;

    // This works because "function" is always set on the URL, even in POSTs.
    $function=$_GET["function"];
    $dbg->info("QUERY String: " . $_SERVER['QUERY_STRING'] );
    if ( $function == "webcam" )
    {
        webcam();
    }
    else if ( $function == "heartbeat" )
    {
        heartbeat();
    }
    else if ( $function == "teardown" )
    {
        teardown();
    }
    else if ( $function == "settings" )
    {
        print settings();
    }
    else if ( $function == "frontpage" )
    {
        $dbg->info("Calling frontPage().");
        frontPage();
    }
    else if ( $function == "savepw" )
    {
        savepw();
    }
    else if ( $function == "users" )
    {
        users();
    }
    else if ( $function == "updateUser" )
    {
        updateUser();
    }
    else if ( $function == "getMACFields" )
    {
        getMACFields();
    }
    else
        print "Unknown function: $function";
        
}

main();

?>
