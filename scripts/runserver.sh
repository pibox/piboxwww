#!/bin/bash -p
# Run a simple web server to test the web service.
# Must be run from the top of the source tree.
# ------------------------------------------------

# ---------------------------------------------
# Initialization
# ---------------------------------------------
DOCROOT=`pwd`/src/piboxwww
DATADIR=$DOCROOT/data

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

# ---------------------------------------------
# functions
# ---------------------------------------------

# Handle CTRL-c (exit from server)
function ctrl_c() 
{
	rm -rf $DATADIR
}

# Show usage message
function doHelp
{
    echo ""
    echo "$0 [-?]"
    echo "Start a PHP web server using the source tree as a document root."
    echo "Automatically starts in debug mode."
}

# ---------------------------------------------
# Process command line options
# ---------------------------------------------
while getopts ":" Option
do
    case $Option in
    *) doHelp; exit 0;;
    esac
done

# ---------------------------------------------
# Main
# ---------------------------------------------

# Create the data directory
mkdir -p $DATADIR

# Add the stamp file for debugging
touch $DATADIR/debug

# Run the server.
php -S galileo:1337 -t $DOCROOT


